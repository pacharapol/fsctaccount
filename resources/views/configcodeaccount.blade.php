<?php
use  App\Api\Connectdb;

?>
@include('headmenu')

<link>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}
<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>
<script type="text/javascript" src = 'js/config/accountcode.js'></script>
<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">

<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-cog home-icon"></i>
                        <a href="#">ตั้งค่า</a>
                    </li>
                    <li class="active">ตั้งค่ารหัสบัญชี</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        ตั้งค่ารหัสบัญชี
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-10">

                                    </div>
                                    <div class="col-md-1">

                                    </div>
                                    <div class="col-md-1">
                                        <a href="#" title="เพิ่มข้อมูล" data-toggle="modal" data-target="#myModal" onclick="insertnew()" ><img src="images/global/add.png"></a>
                                    </div>
                                </div>
                                <div class="row">
                                    <br>
                                </div>
                                <div class="row">
                                    <?php

                                   // print_r($users);
                                    ?>
                                        <table id="example" class="table table-striped table-bordered">
                                            <thead class="thead-inverse">
                                            <tr>
                                                <td>#</td>
                                                <td> รหัสบัญชี </td>
                                                <td>รายละเอียด</td>
                                                <td>สถานะ</td>
                                                <td>การจัดการ</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $db = Connectdb::Databaseall();
                                            $config = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.accounttype');
                                            $i = 1;
                                            ?>
                                            @foreach ($config as $taxdetail)
                                                <tr>
                                                    <td scope="row">{{ $i }}</td>
                                                    <td>{{ $taxdetail->accounttypeno }}</td>
                                                    <td>{{ $taxdetail->accounttypefull }}</td>
                                                    <td><?php if($taxdetail->status==1){ echo "<font color='green'>ใช้งาน</font>";} else {  echo "<font color='red'>ยกเลิก</font>";} ?></td>
                                                    <td><a href="#" title="แก้ไขข้อมูล" data-toggle="modal" data-target="#myModal" onclick="getdata({{ $taxdetail->id }})"><img src="images/global/edit-icon.png"></a>
                                                    </td>

                                                </tr>
                                                <?php $i++; ?>
                                            @endforeach
                                            </tbody>
                                        </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')


<!-- Modal -->

<!-- Modal -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">ตั้งค่ารหัสบัญชี</h5>
            </div>

            <div class="modal-body">
                <!-- The form is placed inside the body of modal -->
                <form id="configFormaccountnumber" onsubmit="return getdatesubmit();" data-toggle="validator" method="post" class="form-horizontal">
                    <input value="{{ null }}" type="hidden" id="id" name="id" />

                    <div class="form-group">
                        <label class="col-xs-3 control-label">รหัสบัญชี<i><span style="color: red">*</span></i></label>
                        <div class="col-xs-5">
                            <input type="text" id="accounttypeno" class="form-control" name="accounttypeno" required/>
                        </div>
                        <div class="col-xs-4">

                        </div>
                    </div>
                    {{--<input value="0" type="hidden" id="check" name="check"/>--}}
                    <div class="form-group">
                        <label class="col-xs-3 control-label">รายละเอียด<i><span style="color: red">*</span></i></label>
                        <div class="col-xs-5">
                            <input type="text" id="accounttypefull" class="form-control" name="accounttypefull" required/>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">สถานะ<i><span style="color: red">*</span></i></label>
                        <div class="col-xs-5">
                           <select class="form-control" name="status" id="status">
                               <option value="1">ใช้งาน</option>
                               <option value="99">ยกเลิก</option>
                           </select>
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-xs-5 col-xs-offset-3">
                            <button type="submit" id="Btn_save" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
