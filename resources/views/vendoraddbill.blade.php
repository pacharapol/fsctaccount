<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;
use  App\Api\Maincenter;


?>
@include('headmenu')
<link>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}

<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>


<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">


<link rel="stylesheet" type="text/css" href="bower_components/select2/dist/css/select2.min.css">
<script type="text/javascript" src = 'bower_components/select2/dist/js/select2.full.min.js'></script>

<script type="text/javascript" src = 'js/vendor/vendoraddbill.js'></script>


<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
{{--<style>--}}
    {{--.modal-ku {--}}
        {{--width: 1024px;--}}
        {{--margin: auto;--}}
    {{--}--}}
{{--</style>--}}
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">งานจัดซื้อ</a>
                    </li>
                    <li class="active">ข้อมูลการจัดซื้อ</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">
                <input type="hidden" id="number_pprset" value="<?php $brcode = Session::get('brcode');
                $db = Connectdb::Databaseall();
                $sql = "SELECT COUNT(id) as idgen FROM $db[fsctaccount].ppr_head WHERE branch_id = '$brcode'";
                $bbr_no_branch = DB::connection('mysql')->select($sql);
                $numberrun = 0;
                //                                        print_r($bbr_no_branch);
                $onset =  $bbr_no_branch[0]->idgen;

                if($onset == 0){
                    $numberrun = 1;
                }else{
                    $numberrun = (int)($onset)+1;
                }
                echo substr(date('Y',strtotime("+543 year")),2,2).date('m').str_pad($numberrun, 3, "0", STR_PAD_LEFT);
                ?>">
                <input type="hidden" id="dateset" value="<?php echo date('Y-m-d')?>">
                <input type="hidden"  class="form-control"  id="yearset" value="<?php echo date('Y')?>"  />
                <input type="hidden"  class="form-control" id="year_thset" value="<?php echo date('Y',strtotime("+543 year"))?>"  />
                <input type="hidden"  class="form-control"  id="bbr_no_branchset" value="<?php
                echo $numberrun;
                ?>"  />
                <input type="hidden"  class="form-control" id="dateshowset" value="<?php echo date('d-m-Y',strtotime("+543 year"))?>"  disabled/>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                        ข้อมูลการจัดซื้อ
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-10">

                                    </div>

                                    <div class="col-md-2">
                                        <a href="#" title="เพิ่มข้อมูล" data-toggle="modal" data-target="#myModal" onclick="insertnew()" ><img src="images/global/add.png">เพิ่มข้อมูล</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <br>
                                </div>
                                        <table id="example" class="table table-striped table-bordered">
                                            <thead class="thead-inverse">
                                            <tr>
                                                <td>#</td>
                                                <td>วันที่</td>
                                                <td>PPR No.</td>
                                                <td>รายชื่อ Vendor</td>
                                                <td>สาขา</td>
                                                <td>แก้ไข</td>
                                                <td>สถานะ</td>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $db = Connectdb::Databaseall();
                                            $data = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.ppr_head ');
                                            $i = 1;
                                            ?>
                                            @foreach ($data as $value)
                                                <tr>
                                                    <td scope="row">{{ $i }}</td>
                                                    <td align="left">{{ $value->date}}</td>
                                                    <td align="center">{{ $value->number_ppr }}</td>
                                                    <td align="center">{{ $value->supplier_id }}</td>
                                                    <td align="center">{{ $value->branch_id }}</td>
                                                    <td align="center">
                                                        <a href="#" title="แก้ไขข้อมูล" data-toggle="modal" data-target="#myModal" onclick="getdata({{ $value->id }})"><img src="images/global/edit-icon.png"></a>
                                                    </td>
                                                    <td align="center">
                                                        {{ $value->status }}
                                                    </td>
                                                </tr>
                                                <?php $i++; ?>
                                            @endforeach
                                            </tbody>
                                        </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')


<!-- Modal -->

<div class="modal fade" id="myModal"  role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog"  style="width:1250px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">ข้อมูลใบ PPR</h5>
            </div>

            <div class="modal-body">

                <form id="configFormvendors" onsubmit="return getdatesubmit();" data-toggle="validator" method="post" class="form-horizontal">
                    <input value="{{ null }}" type="hidden" id="id" name="id" />
                    <input type="hidden" name="status" id="status" value="0">
                    <input type="hidden" id="statusprocess" value="0">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-right">รหัสสาขา <?php echo $brcode = Session::get('brcode');?>
                                <input type="hidden"  class="form-control" name="branch_id" id="branch_id" value="<?php echo $brcode ;?>"  />
                                (
                                <?php
                                $databr = Maincenter::databranchbycode($brcode);
                                print_r($databr[0]->name_branch);
                                ?>)
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-3 control-label">วันที่ขอ<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-5">
                                    <input type="text"  class="form-control" id="dateshow" value="<?php echo date('d-m-Y',strtotime("+543 year"))?>"  disabled/>
                                    <input type="hidden"  class="form-control" name="date" id="date" value="<?php echo date('Y-m-d')?>"  />
                                    <input type="hidden"  class="form-control" name="year" id="year" value="<?php echo date('Y')?>"  />
                                    <input type="hidden"  class="form-control" name="year_th" id="year_th" value="<?php echo date('Y',strtotime("+543 year"))?>"  />
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>

                        <div class="col-md-8">
                            <div class="pull-right">
                                <div class="form-group">
                                    <label class="col-xs-6 control-label">PPR No.<i><span style="color: red">*</span></i></label>
                                    <div class="col-xs-5">
                                        <input type="text"  class="form-control" name="number_ppr" id="number_ppr" value=""  readonly/>

                                        <input type="hidden"  class="form-control" name="bbr_no_branch" id="bbr_no_branch" value=""  readonly/>

                                    </div>
                                    <div class="col-xs-4">

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">เลือก Vendor<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                   <select class=" select2" id="supplier_id" name="supplier_id" required>
                                       <option value="">เลือก vendor</option>
                                       <?php
                                       $db = Connectdb::Databaseall();
                                       $data = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.supplier');
                                       ?>
                                       @foreach ($data as $value)
                                           <option value="{{$value->id}}">{{$value->pre}}  {{$value->name_supplier}}</option>

                                       @endforeach


                                   </select>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">ประเภทการจ่าย<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <select class="form-control" id="type_pay" name="type_pay" required>
                                        <option value="">เลือก ประเภทการจ่าย</option>
                                        <?php
                                        $db = Connectdb::Databaseall();
                                        $data = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.type_pay WHERE status ="1"');
                                        ?>
                                        @foreach ($data as $value)
                                            <option value="{{$value->id}}">{{$value->name}}</option>

                                        @endforeach


                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">ประเภทการซื้อ<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <select class="form-control" id="type_buy" name="type_buy" required>
                                        <option value="">เลือก ประเภทการซื้อ</option>
                                        <?php
                                        $db = Connectdb::Databaseall();
                                        $data = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.type_buy  WHERE status ="1"');
                                        ?>
                                        @foreach ($data as $value)
                                            <option value="{{$value->id}}">{{$value->name}}</option>

                                        @endforeach


                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">การจัดซื้อ(ใน/นอก ประเทศ)<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input  type="radio" name="in_house" id="in_house1" value="1" checked>  ภายในประเทศ
                                    <br>
                                    <input  type="radio" name="in_house" id="in_house2" value="2" >  ต่างประเทศ
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">งบประมาณ<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input  type="radio" name="in_budget" id="in_budget1" value="1" checked>  ภายในงบประมาณ
                                    <br>
                                    <input  type="radio" name="in_budget" id="in_budget2" value="2" >  นอกงบประมาณ
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">การจัดซื้อ(เร่งด่วน/ปกติ)<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input  type="radio" name="urgent_status" id="urgent_status1" value="1" >  เร่งด่วน
                                    <br>
                                    <input  type="radio" name="urgent_status" id="urgent_status2" value="2" checked>  ปกติ
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-right">
                                <a href="#" title="เพิ่มรายการ" id="addrow" ><img src="images/global/add.png">เพิ่มรายการ</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <table class="table" id="thdetail">
                                <thead>
                                <tr>
                                    <td align="center">ประเภทงานจัดซื้อ</td>
                                    <td align="center">รายการ</td>
                                    <td align="center" colspan="2">จำนวนนับ</td>
                                    <td align="center">ราคาต่อหน่วย</td>
                                    <td align="center">vat</td>
                                    <td align="center">รวม</td>
                                    <td align="center">ซื้อล่าสุด</td>
                                    <td align="center">ค่าเฉลี่ย</td>
                                    <td align="center">หมายเหตุ</td>
                                    <td align="center">ลบ</td>
                                </tr>
                                </thead>
                                <tbody id="tdbody">
                                 <tr>
                                     <td align="center">
                                         <select name="config_group_supp_id[]" id="config_group_supp_id0" class="form-control" required>
                                             <option value="">เลือกประเภทการจัดซื้อ</option>
                                             <?php
                                             $db = Connectdb::Databaseall();
                                             $data = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.config_group_supp WHERE status = "1"');
                                             ?>
                                             @foreach ($data as $value)
                                                 <option value="{{$value->id}}">{{$value->name}}</option>
                                             @endforeach
                                         </select>

                                     </td>
                                     <td align="center">
                                         <input type="text" name="list[]" id="list0"  class="form-control">
                                     </td>
                                     <td align="center">
                                         <input type="text" name="amount[]" id="amount0" onblur="getamount(this,0)" class="form-control" style="width: 90px;" placeholder="จำนวน">
                                     </td>
                                     <td align="center">
                                         <input type="text" name="type_amount[]" id="type_amount0"  class="form-control" style="width: 80px;" placeholder="หน่วยนับ">
                                     </td>
                                     <td align="center">
                                         <input type="text" name="price[]" id="price0" onblur="getprice(this,0)"  disabled  class="form-control" >
                                     </td>
                                     <td align="center">
                                         <select name="vat[]" id="vat0" class="form-control" onchange="calculatevat(this,0)" style="width: 90px;" >
                                             <option value="0">ไม่มี vat</option>
                                             <?php
                                             $db = Connectdb::Databaseall();
                                             $data = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.tax_config WHERE status = "1"');
                                             ?>
                                             @foreach ($data as $value)
                                                 <option value="{{$value->tax}}">{{$value->tax}}</option>
                                             @endforeach
                                         </select>
                                     </td>
                                     <td align="center">
                                         <input type="text" name="total[]"  id="total0" class="form-control"  >
                                     </td>
                                     <td align="center">
                                         <input type="text" name="daterecentpurchases[]" readonly id="daterecentpurchases0"  class="form-control datepicker"  >
                                     </td>
                                     <td align="center">
                                         <input type="text" name="avg[]" id="avg0"  class="form-control"  >
                                     </td>
                                     <td align="center">
                                         <input type="text" name="note[]" id="note0"  class="form-control"  >
                                     </td>
                                     <td align="center"></td>
                                 </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-right">
                            <u>รหัสพนักงาน: <?php echo $brcode = Session::get('emp_code')?>
                                <input type="hidden" class="form-control" name="code_emp" id="code_emp" value="<?php echo $brcode = Session::get('emp_code')?>">
                            </u>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-right">
                                <input type="text" class="form-control" name="code_sup" id="code_sup" placeholder="รหัสซุปประจำสาขา">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-xs-5 col-xs-offset-3">
                                    <button type="submit" id="Btn_save" class="btn btn-primary">Save</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>



