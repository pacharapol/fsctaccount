<?php
use  App\Api\Connectdb;
use  App\Api\Accountcenter;

?>
@include('headmenu')
<link>
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>--}}
{{--<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>--}}
<script type="text/javascript" src = 'js/bootbox.min.js'></script>
<script type="text/javascript" src = 'js/validator.min.js'></script>

<script type="text/javascript" src = 'js/jquery.dataTables.min.js'></script>
<script type="text/javascript" src = 'js/dataTables.bootstrap.min.js'></script>
<link rel="stylesheet" type="text/css" href="css/table/dataTables.bootstrap.min.css">

<script type="text/javascript" src = 'js/vendor/vendorcenter.js'></script>


<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<style>
    .modal-ku {
        width: 90%;
        margin: auto;
    }
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">งานจัดซื้อ</a>
                    </li>
                    <li class="active">ข้อมูล Vendors</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="breadcrumbs" id="breadcrumbs">
                                <ul class="breadcrumb">
                                    <li>
                                     รายนาม Vendors
                                    </li>
                                </ul><!-- /.breadcrumb -->
                                <!-- /section:basics/content.searchbox -->
                            </div>
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="pull-right">
                                            <img src="images/global/invoice.png">เพิ่มรายการจัดซื้อ
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <a href="#" title="เพิ่มข้อมูล" data-toggle="modal" data-target="#myModal" onclick="insertnew()" ><img src="images/global/add.png">เพิ่มข้อมูล</a>
                                    </div>
                                </div>

                                        <table id="example" class="table table-striped table-bordered">
                                            <thead class="thead-inverse">
                                            <tr>
                                                <td>#</td>
                                                <td>ชื่อบริษัท</td>
                                                <td>สาขา</td>
                                                <td>เบอร์โทร</td>
                                                <td>มือถือ</td>
                                                <td>Credit Terms</td>
                                                <td>แก้ไข</td>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                $db = Connectdb::Databaseall();
                                                $data = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.supplier');
                                                $i = 1;
                                                ?>
                                                @foreach ($data as $value)
                                                    <tr>
                                                        <td scope="row">{{ $i }}</td>
                                                        <td align="left">{{ $value->pre }}  {{ $value->name_supplier }}</td>
                                                        <td align="center">{{ $value->type_branch }}</td>
                                                        <td align="center">{{ $value->phone }}</td>
                                                        <td align="center">{{ $value->mobile }}</td>
                                                        <td align="center">
                                                            <?php
                                                            $dataterms = Accountcenter::dataterms($value->terms_id);
                                                            print_r($dataterms[0]->name);
                                                            ?>
                                                        </td>

                                                        <td align="center">
                                                            <a href="#" title="แก้ไขข้อมูล" data-toggle="modal" data-target="#myModal" onclick="getdata({{ $value->id }})"><img src="images/global/edit-icon.png"></a>
                                                        </td>
                                                    </tr>
                                                    <?php $i++; ?>
                                                @endforeach
                                            </tbody>
                                        </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')


<!-- Modal -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">ข้อมูล vendor</h5>
            </div>

            <div class="modal-body">
                <form id="configFormvendors" onsubmit="return getdatesubmit();" data-toggle="validator" method="post" class="form-horizontal">
                    <input value="{{ null }}" type="hidden" id="id" name="id" />


                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-xs-4 control-label">คำนำหน้า<i><span style="color: red">*</span></i></label>
                            <div class="col-xs-6">
                               <select name="pre" id="pre" class="form-control">
                                   <?php
                                   $db = Connectdb::Databaseall();
                                   $dataper = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.initial');
                                   ?>
                                           <option value="">เลือกคำนำหน้า</option>
                                       @foreach ($dataper as $value)
                                           <option value="{{ $value->per}}">{{ $value->per}}</option>
                                       @endforeach

                               </select>
                            </div>
                            <div class="col-xs-4">

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-xs-4 control-label">ชื่อบริษัท/ชื่อ<i><span style="color: red">*</span></i></label>
                            <div class="col-xs-6">
                               <input type="text" class="form-control" name="name_supplier" id="name_supplier">
                            </div>
                            <div class="col-xs-4">

                            </div>
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">รหัสสาขา<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="type_branch" id="type_branch">
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">บ้าน เลขที่/หมู่ที่<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="address" id="address">
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">ตำบล<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="district" id="district">
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">อำเภอ<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="amphur" id="amphur">
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">จังหวัด<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="province" id="province">
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">รหัสไปรษณีย์<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="zipcode" id="zipcode">
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">เบอร์โทร<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="phone" id="phone">
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">มือถือ<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="mobile" id="mobile">
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">e-mail<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="email" id="email">
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">รหัสผู้เสียภาษี<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="tax_id" id="tax_id">
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Credit Terms<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">

                                    <select name="terms_id" id="terms_id" class="form-control">
                                        <?php
                                        $db = Connectdb::Databaseall();
                                        $dataterms = DB::connection('mysql')->select('SELECT * FROM '.$db['fsctaccount'].'.supplier_terms');
                                        ?>
                                        <option value="">เลือก Credit Terms</option>
                                        @foreach ($dataterms as $value)
                                            <option value="{{ $value->id}}">{{ $value->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">หมายเหตุ<i><span style="color: red">*</span></i></label>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control" name="Note" id="Note">
                                </div>
                                <div class="col-xs-4">

                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="row">
                            <br>
                        </div>
                        <div class="row">

                            <div class="col-md-4">

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="col-xs-5 col-xs-offset-3">
                                        <button type="submit" id="Btn_save" class="btn btn-primary">Save</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>
