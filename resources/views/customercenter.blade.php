@include('headmenu')
<link>

<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.js'></script>
<script type="text/javascript" src = 'js/jquery-ui-1.12.1/jquery-ui.min.js'></script>






{{--<script type="text/javascript" src = 'js/bootbox.min.js'></script>--}}

{{--<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">--}}
{{--<!-- Font Awesome -->--}}
{{--<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">--}}
{{--<!-- Ionicons -->--}}
{{--<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">--}}
{{--<!-- Theme style -->--}}
{{--<link rel="stylesheet" href="dist/css/AdminLTE.min.css">--}}
{{--<!-- AdminLTE Skins. Choose a skin from the css/skins--}}
     {{--folder instead of downloading all of them to reduce the load. -->--}}
{{--<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">--}}
{{--<!-- Morris chart -->--}}
{{--<link rel="stylesheet" href="bower_components/morris.js/morris.css">--}}
{{--<!-- jvectormap -->--}}
{{--<link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">--}}
<!-- Date Picker -->
<link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<!-- Daterange picker -->
<link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
<!-- bootstrap wysihtml5 - text editor -->
{{--<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">--}}


{{--<!-- jQuery 3 -->--}}
{{--<script src="bower_components/jquery/dist/jquery.min.js"></script>--}}
{{--<!-- jQuery UI 1.11.4 -->--}}
{{--<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>--}}
{{--<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->--}}

{{--<!-- Bootstrap 3.3.7 -->--}}
{{--<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>--}}
{{--<!-- Morris.js charts -->--}}
{{--<script src="bower_components/raphael/raphael.min.js"></script>--}}
{{--<script src="bower_components/morris.js/morris.min.js"></script>--}}
{{--<!-- Sparkline -->--}}
{{--<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>--}}
{{--<!-- jvectormap -->--}}
{{--<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>--}}
{{--<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>--}}
{{--<!-- jQuery Knob Chart -->--}}
{{--<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>--}}
{{--<!-- daterangepicker -->--}}
{{--<script src="bower_components/moment/min/moment.min.js"></script>--}}
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
{{--<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>--}}
{{--<!-- Slimscroll -->--}}
{{--<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>--}}
{{--<!-- FastClick -->--}}
{{--<script src="bower_components/fastclick/lib/fastclick.js"></script>--}}
{{--<!-- AdminLTE App -->--}}
{{--<script src="dist/js/adminlte.min.js"></script>--}}
{{--<!-- AdminLTE dashboard demo (This is only for demo purposes) -->--}}
{{--<script src="dist/js/pages/dashboard.js"></script>--}}
{{--<!-- AdminLTE for demo purposes -->--}}
{{--<script src="dist/js/demo.js"></script>--}}


<script type="text/javascript" src = 'js/validator.min.js'></script>
<script type="text/javascript" src = 'js/customer/customer.js'></script>
<script type="text/javascript" src = 'src/jquery.mask.js'></script>

<meta name="csrf-token" content="{{ csrf_token() }}" />
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->


    <section class="content">
        <div class="box box-success">
            <div class="breadcrumbs" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="#">Customer</a>
                    </li>
                    <li class="active">Customer Center</li>
                </ul><!-- /.breadcrumb -->
                <!-- /section:basics/content.searchbox -->
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-2">
                        <select class="form-control">
                            <option value="">New Customer & Job</option>
                            <option value="">Add Job</option>
                            <option value="">Add Multiple Customer Jobs</option>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select class="form-control">
                            <option value="">New Transaction</option>
                            <option value="">Estimates</option>
                            <option value="">Invoices</option>
                            <option value="">Sales Receipts</option>
                            <option value="">Statement Charges</option>
                            <option value="">Receive Payments</option>
                            <option value="">Credit Memos/Refunds</option>
                        </select>
                    </div>
                    <div class="col-md-1">
                        <select class="form-control">
                            <option value="">Print</option>
                            <option value="">Customer & Job List</option>
                            <option value="">Customer & Job Information</option>
                            <option value="">Customer & Job Transaction List</option>
                        </select>
                    </div>
                    <div class="col-md-1">
                        <select class="form-control">
                            <option value="">Excel</option>
                            <option value="">Export Customer List</option>
                            <option value="">Export Transaction</option>
                            <option value="">Import from Excel</option>
                            <option value="">Paste from Excel</option>
                        </select>
                    </div>
                    <div class="col-md-1">
                        <select class="form-control">
                            <option value="">Word</option>
                            <option value="">Prepare Letter to ds dsad</option>
                            <option value="">Prepare Customer Letters</option>
                            <option value="">Prepare Collection Letters</option>
                            <option value="">Customize Letter Templates</option>
                        </select>
                    </div>
                    <div class="col-md-5">

                    </div>
                </div>
                <div class="row">
                    <br>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="box box-primary">
                            <div class="box-body">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#home" onclick="selectmenu(1)">Customers & Jobs</a></li>
                                    <li><a data-toggle="tab" href="#menu1" onclick="selectmenu(2)">Transactions</a></li>

                                </ul>

                                    <div class="tab-content">
                                        <div id="home" class="tab-pane fade in active">
                                            <div class="box-body">
                                                <div class="row">
                                                   <div class="col-md-9">
                                                       <select class="form-control" name="typecustommer">
                                                           <option value="">All Customers</option>
                                                           <option value="">Active Customers</option>
                                                           <option value="">Customers with Open Balance</option>
                                                           <option value="">Customers with Overdue Invoices</option>
                                                           <option value="">Customers with Almost Due Invoice</option>
                                                           <option value="">Customers Fiter</option>
                                                       </select>
                                                   </div>
                                                    <div class="col-md-3">

                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <br>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <input type="text" class="form-control" name="customername_s" id="customername_s">

                                                    </div>
                                                    <div class="col-md-1">
                                                        <img src="images/global/search.png">
                                                    </div>
                                                    <div class="col-md-2">

                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <br>
                                                </div>
                                                <div class="row">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>Name</th>
                                                                <th>Company</th>
                                                                <th>Balance Total</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @for($i = 0; $i < 10; $i++)
                                                            <tr height="30">
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            @endfor
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="menu1" class="tab-pane fade">
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a href="#" onclick="selecttransaction('Estimates')">
                                                            <i class="fa fa-book"></i><span> Estimates</span>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <a href="#" onclick="selecttransaction('Invoices')">
                                                        <i class="fa fa-book"></i><span> Invoices</span>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <a href="#" onclick="selecttransaction('Statement')">
                                                        <i class="fa fa-book"></i><span> Statement Charges</span>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <a href="#" onclick="selecttransaction('Sales')">
                                                        <i class="fa fa-book"></i><span> Sales Receipts</span>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <a href="#"  onclick="selecttransaction('Received')">
                                                        <i class="fa fa-book"></i><span> Received Payments</span>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <a href="#"  onclick="selecttransaction('Credit')">
                                                        <i class="fa fa-book"></i><span> Credit Memos</span>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <a href="#"  onclick="selecttransaction('Refunds')">
                                                        <i class="fa fa-book"></i><span> Refunds</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="box box-primary">
                            <div class="box-body" id="pageA">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="breadcrumbs" id="breadcrumbs">
                                            <ul class="breadcrumb">
                                                <li>
                                                    <h4>Customer Information</h4>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Company Name
                                                </div>
                                                <div class="col-md-8">
                                                    <span id="companyname"></span>
                                                </div>
                                            </div>
                                            <div class="row">
                                               <br>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                   Full Name
                                                </div>
                                                <div class="col-md-8">
                                                    <span id="fullname"></span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <br>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    Bill To
                                                </div>
                                                <div class="col-md-8">
                                                    <span id="billto"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="breadcrumbs" id="breadcrumbs">
                                            <ul class="breadcrumb">
                                                <li>
                                                    <h6>Reports For This Customer</h6>
                                                </li>
                                            </ul>
                                            <div class="box-body">
                                                <div class="row">
                                                    <a href="#">QuickReport</a>
                                                </div>
                                                <div class="row">
                                                    <a href="#">Open Balance</a>
                                                </div>
                                                <div class="row">
                                                    <a href="#">Show Estimates</a>
                                                </div>
                                                <div class="row">
                                                    <a href="#">Customer Snapshot</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="box-body">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#Transactions">Transactions</a></li>
                                            <li><a data-toggle="tab" href="#Contacts">Contacts</a></li>
                                            <li><a data-toggle="tab" href="#Todos">To Do's</a></li>
                                            <li><a data-toggle="tab" href="#Notes">Notes</a></li>
                                        </ul>
                                        <div class="tab-content">

                                            <div id="Transactions" class="tab-pane fade in active">
                                                <div class=" box-body">
                                                    <div class="row">
                                                        <div class="col-md-1">
                                                            SHOW
                                                        </div>
                                                        <div class="col-md-2">
                                                            <select class="form-control">
                                                                <option value="">All Transactions</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-2">
                                                            FILTER BY
                                                        </div>
                                                        <div class="col-md-2">
                                                            <select class="form-control">
                                                                <option value="">All</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-1">
                                                            DATE
                                                        </div>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control reservation" >
                                                        </div>
                                                        <div class="col-md-1">

                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <br>
                                                    </div>
                                                    <div class="row">
                                                        <table class="table table-striped table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>TYPE</th>
                                                                <th>DATE</th>
                                                                <th>NUM</th>
                                                                <th>DUE DATE</th>
                                                                <th>AGING</th>
                                                                <th>AMOUNT</th>
                                                                <th>OPEN BALANCE</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @for($j = 0; $j < 5; $j++)
                                                                <tr height="30">
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                            @endfor
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>
                                            </div>
                                            <div id="Contacts" class="tab-pane fade">
                                                <div class=" box-body">
                                                    <table class="table table-striped table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th width="30%">CONTACT NAME</th>
                                                            <th>CONTACT INFO</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @for($j = 0; $j < 3; $j++)
                                                            <tr height="50">
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                        @endfor
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div id="Todos" class="tab-pane fade">
                                                <div class=" box-body">
                                                    <div class="row">
                                                        <div class="col-md-1">
                                                            TYPE
                                                        </div>
                                                        <div class="col-md-2">
                                                            <select class="form-control">
                                                                <option value="All">All</option>
                                                                <option value="Call">Call</option>
                                                                <option value="Fax">Fax</option>
                                                                <option value="E-mail">E-mail</option>
                                                                <option value="Meeting">Meeting</option>
                                                                <option value="Appointment">Appointment</option>
                                                                <option value="Task">Task</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-2">
                                                            STATUS
                                                        </div>
                                                        <div class="col-md-2">
                                                            <select class="form-control">
                                                                <option value="All">All</option>
                                                                <option value="Active">Active</option>
                                                                <option value="Done">Done</option>
                                                                <option value="Inactive">Inactive</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-1">
                                                            DATE
                                                        </div>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control reservation" >
                                                        </div>
                                                        <div class="col-md-1">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <br>
                                                    </div>
                                                    <div class="row">
                                                        <table class="table table-striped table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>DONE</th>
                                                                <th>PRIORITY</th>
                                                                <th>DATE</th>
                                                                <th>TYPE</th>
                                                                <th>DETAILS</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @for($j = 0; $j < 5; $j++)
                                                                <tr height="30">
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                            @endfor
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="Notes" class="tab-pane fade">
                                                <div class=" box-body">
                                                    <div class="row">
                                                        <div class="col-md-1">
                                                            DATE
                                                        </div>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control reservation" >
                                                        </div>
                                                        <div class="col-md-8">

                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <br>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-9">
                                                            <table class="table table-striped table-bordered">
                                                                <thead>
                                                                <tr>
                                                                    <th width="30%">DONE</th>
                                                                    <th>NOTES</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @for($j = 0; $j < 5; $j++)
                                                                    <tr height="30">
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                @endfor
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <textarea rows="10" readonly>

                                                            </textarea>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body" id="pageB" style="display: none">
                                <div class=" box-body" id="Estimates">
                                    <div class="row">
                                       <h3>Estimates</h3>
                                    </div>
                                    <div class="row">
                                        <br>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            FILTER BY
                                        </div>
                                        <div class="col-md-2">
                                            <select class="form-control">
                                                <option value="All">All Credit Memos</option>
                                                <option value="Call">Open Credit Memos</option>
                                            </select>
                                        </div>
                                        <div class="col-md-1">
                                            DATE
                                        </div>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control reservation" >
                                        </div>
                                        <div class="col-md-4">

                                        </div>


                                    </div>
                                    <div class="row">
                                        <br>
                                    </div>
                                    <div class="row">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <th>CUSTOMER</th>
                                                <th>NUM</th>
                                                <th>DATE</th>
                                                <th>ACTIVE ESTIMATE</th>
                                                <th>AMOUNT</th>
                                                <th>OPEN BALANCE</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @for($j = 0; $j < 10; $j++)
                                                <tr height="30">
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            @endfor
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class=" box-body" id="Invoices">
                                    <div class="row">
                                        <h3>Invoices</h3>
                                    </div>
                                    <div class="row">
                                        <br>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            FILTER BY
                                        </div>
                                        <div class="col-md-2">
                                            <select class="form-control">
                                                <option value="All">All Invoices</option>
                                                <option value="OpenInvoices">Open Invoices</option>
                                                <option value="OverdueInvoices">Overdue Invoices</option>
                                            </select>
                                        </div>
                                        <div class="col-md-1">
                                            DATE
                                        </div>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control reservation" >
                                        </div>
                                        <div class="col-md-4">

                                        </div>


                                    </div>
                                    <div class="row">
                                        <br>
                                    </div>
                                    <div class="row">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <th>CUSTOMER</th>
                                                <th>NUM</th>
                                                <th>DATE</th>
                                                <th>ACTIVE ESTIMATE</th>
                                                <th>AMOUNT</th>
                                                <th>OPEN BALANCE</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @for($j = 0; $j < 10; $j++)
                                                <tr height="30">
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            @endfor
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class=" box-body" id="Statement">
                                    <div class="row">
                                        <h3>Statement Charges</h3>
                                    </div>
                                    <div class="row">
                                        <br>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            FILTER BY
                                        </div>
                                        <div class="col-md-2">
                                            <select class="form-control">
                                                <option value="All">All Statement Charges</option>
                                                <option value="OpenInvoices">Open Statement Charges</option>
                                                <option value="OverdueInvoices">Overdue Statement Charges</option>
                                            </select>
                                        </div>
                                        <div class="col-md-1">
                                            DATE
                                        </div>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control reservation" >
                                        </div>
                                        <div class="col-md-4">

                                        </div>


                                    </div>
                                    <div class="row">
                                        <br>
                                    </div>
                                    <div class="row">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <th>CUSTOMER</th>
                                                <th>NUM</th>
                                                <th>DATE</th>
                                                <th>ACTIVE ESTIMATE</th>
                                                <th>AMOUNT</th>
                                                <th>OPEN BALANCE</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @for($j = 0; $j < 10; $j++)
                                                <tr height="30">
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            @endfor
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class=" box-body" id="Sales">
                                    <div class="row">
                                        <h3>Sales Receipts</h3>
                                    </div>
                                    <div class="row">
                                        <br>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            FILTER BY
                                        </div>
                                        <div class="col-md-2">
                                            <select class="form-control">
                                                <option value="All">All Sales Receipts</option>
                                            </select>
                                        </div>
                                        <div class="col-md-1">
                                            DATE
                                        </div>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control reservation" >
                                        </div>
                                        <div class="col-md-4">

                                        </div>


                                    </div>
                                    <div class="row">
                                        <br>
                                    </div>
                                    <div class="row">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <th>CUSTOMER</th>
                                                <th>NUM</th>
                                                <th>DATE</th>
                                                <th>ACTIVE ESTIMATE</th>
                                                <th>AMOUNT</th>
                                                <th>OPEN BALANCE</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @for($j = 0; $j < 10; $j++)
                                                <tr height="30">
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            @endfor
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class=" box-body" id="Received">
                                    <div class="row">
                                        <h3>Received Payments</h3>

                                    </div>
                                    <div class="row">
                                        <br>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            FILTER BY
                                        </div>
                                        <div class="col-md-2">
                                            <select class="form-control">
                                                <option value="All">All Sales Receipts</option>
                                            </select>
                                        </div>
                                        <div class="col-md-1">
                                            DATE
                                        </div>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control reservation" >
                                        </div>
                                        <div class="col-md-4">

                                        </div>


                                    </div>
                                    <div class="row">
                                        <br>
                                    </div>
                                    <div class="row">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <th>CUSTOMER</th>
                                                <th>NUM</th>
                                                <th>DATE</th>
                                                <th>ACTIVE ESTIMATE</th>
                                                <th>AMOUNT</th>
                                                <th>OPEN BALANCE</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @for($j = 0; $j < 10; $j++)
                                                <tr height="30">
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            @endfor
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@include('footer')

