<?php


use Illuminate\Support\Facades\Input;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $data = Input::all();
//
    Session::put('brcode', $data['brcode']);
    Session::put('emp_code', $data['pw_login']);

    return view('welcome');
});
Route::get('/mainmenu', function () {
    return view('tempmenu');
});

Route::get('/mainmenuhead', function () {
    return view('headmenu');
});

Route::get('/mainmenufooter', function () {
    return view('footer');
});

Route::get('/listsform','ListsController@form');

Route::get('/listsform','ListsController@form');

Route::get('/companyinformation','CompanyController@companyinformation');

Route::get('/companyinformationbyid/{id}','CompanyController@companyinformationbyid');

Route::post('/companyinsert','CompanyController@companyinsert');

Route::post('/deletecompany','CompanyController@deletecompany');

Route::get('/customercenter','CustomerController@customercenter');

Route::get('/configaccounttax','ConfigaccountController@taxfig');

Route::post('/configtaxinsertandupdate','ConfigaccountController@configtaxinsertandupdate');

Route::get('/getdataconfigtax','ConfigaccountController@getdataconfigtax');

Route::get('/configcodeaccount','ConfigaccountController@configcodeaccount');

Route::post('/configaccoutcodeinsertandupdate','ConfigaccountController@configaccoutcodeinsertandupdate');

Route::get('/getdataconfigaccountcode','ConfigaccountController@getdataconfigaccountcode');

Route::get('/configtypepay','ConfigaccountController@configtypepay');

Route::post('/configtypepayinsertandupdate','ConfigaccountController@configtypepayinsertandupdate');

Route::get('/getdataconfigtypepay','ConfigaccountController@getdataconfigtypepay');

Route::get('/configtypebuy','ConfigaccountController@configtypebuy');

Route::post('/configtypebuyinsertandupdate','ConfigaccountController@configtypebuyinsertandupdate');

Route::get('/getdataconfigtypebuy','ConfigaccountController@getdataconfigtypebuy');

Route::get('/configterms','ConfigaccountController@configterms');

Route::post('/configtermsinsertandupdate','ConfigaccountController@configtermsinsertandupdate');

Route::get('/getdataconfigterms','ConfigaccountController@getdataconfigterms');

Route::get('/configconfiggroupsupp','ConfigaccountController@configconfiggroupsupp');

Route::post('/configconfiggroupsuppinsertandupdate','ConfigaccountController@configconfiggroupsuppinsertandupdate');

Route::get('/getdataconfigconfiggroupsupp','ConfigaccountController@getdataconfigconfiggroupsupp');

Route::get('/vendorcenter','VendorController@vendorcenter');

Route::post('/vendorcenterinsertandupdate','VendorController@vendorcenterinsertandupdate');

Route::get('/getdatavendorcenter','VendorController@getdatavendorcenter');

Route::get('/vendoraddbill','VendorController@vendoraddbill');

Route::post('/insertheadppr','VendorController@insertheadppr');

Route::post('/insertdetailppr','VendorController@insertdetailppr');

Route::get('/getdatavendordetail','VendorController@getdatavendordetail');

Route::get('/getdatavendorpprdetail','VendorController@getdatavendorpprdetail');

Route::get('/configinitial','ConfigaccountController@configinitial');

Route::post('/configinitialinsertandupdate','ConfigaccountController@configinitialinsertandupdate');

Route::get('/getdataconfiginitial','ConfigaccountController@getdataconfiginitial');




