<?php
/**
 * Created by PhpStorm.
 * User: pacharapol
 * Date: 1/14/2018 AD
 * Time: 7:06 PM
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Api\Connectdb;

use App\Pprdetail;
use DB;
use Illuminate\Support\Facades\Input;


class VendorController extends  Controller
{
    public function vendorcenter(){
        return view('vendorcenter');
    }

    public function vendorcenterinsertandupdate(){
        $data= Input::all();
        $db = Connectdb::Databaseall();
        $datainsert = json_decode($data['data']);
        $arrInsert = [];

        foreach ($datainsert as $v){
            $arrInsert [$v->name] = $v->value;
        }
//        print_r($arrInsert);

        if($arrInsert['id'] == ""){
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'supplier')->insert($arrInsert);
        }else{
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'supplier')->where('id',$arrInsert['id'])->update($arrInsert);
        }

        return 1;
    }

    public function getdatavendorcenter(){
        $data = Input::all();

        $db = Connectdb::Databaseall();
        $sql = "SELECT $db[fsctaccount].supplier.* 
                     FROM $db[fsctaccount].supplier
                     WHERE $db[fsctaccount].supplier.id = '$data[id]'";

        $dataquery = DB::connection('mysql')->select($sql);

        return $dataquery;

    }
    public function vendoraddbill(){
        return view('vendoraddbill');
    }


    public function insertheadppr(){
        $data= Input::all();
        $db = Connectdb::Databaseall();

//        print_r($data);
//        exit;
        if($data['data']['id'] == ""){

            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'ppr_head')->insert($data['data']);
            return $lastid = DB::getPdo()->lastInsertId();

        }else{
            $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'ppr_head')->where('id',$data['data']['id'])->update($data['data']);

            return $data['data']['id'];

        }





    }

    public function insertdetailppr(){

        $data= Input::all();
        $db = Connectdb::Databaseall();
        $idhead = $data['data']['idhead'];
        $sql ='SELECT * FROM '.$db['fsctaccount'].'.ppr_detail WHERE ppr_headid = "'.$idhead.'" AND status ="1"';
        $model = DB::connection('mysql')->select($sql);
        $newarr = $data['data'];
//
//        print_r($model);
//        exit;

        if(!$model){
            //print_r($data);

            $arrvalue = [];


                foreach ($newarr['config_group_supp_id'] as $k =>$v){
                    $arrvalue[$k]['config_group_supp_id']= $v;
                    $arrvalue[$k]['list']= $newarr['arrlist'][$k];
                    $arrvalue[$k]['amount']= $newarr['arramount'][$k];
                    $arrvalue[$k]['type_amount']= $newarr['arrtype_amount'][$k];
                    $arrvalue[$k]['price']= $newarr['arrprice'][$k];
                    $arrvalue[$k]['vat']= $newarr['arrvat'][$k];
                    $arrvalue[$k]['total']= $newarr['arrtotal'][$k];
                    $arrvalue[$k]['daterecentpurchases']= $newarr['arrdaterecentpurchases'][$k];
                    $arrvalue[$k]['avg']= $newarr['arravg'][$k];
                    $arrvalue[$k]['note']= $newarr['arrnote'][$k];
                    $arrvalue[$k]['status']= 1;
                    $arrvalue[$k]['ppr_headid']= $newarr['idhead'];
                }

                $model = DB::connection('mysql')->table($db['fsctaccount'].'.'.'ppr_detail')->insert($arrvalue);


        }else{

            foreach ($newarr['config_group_supp_id'] as $k =>$v){
                $arrvalue[$k]['config_group_supp_id']= $v;
                $arrvalue[$k]['list']= $newarr['arrlist'][$k];
                $arrvalue[$k]['amount']= $newarr['arramount'][$k];
                $arrvalue[$k]['type_amount']= $newarr['arrtype_amount'][$k];
                $arrvalue[$k]['price']= $newarr['arrprice'][$k];
                $arrvalue[$k]['vat']= $newarr['arrvat'][$k];
                $arrvalue[$k]['total']= $newarr['arrtotal'][$k];
                $arrvalue[$k]['daterecentpurchases']= $newarr['arrdaterecentpurchases'][$k];
                $arrvalue[$k]['avg']= $newarr['arravg'][$k];
                $arrvalue[$k]['note']= $newarr['arrnote'][$k];
                $arrvalue[$k]['status']= 1;
                $arrvalue[$k]['ppr_headid']= $newarr['idhead'];
            }

            $modelupdate = DB::connection('mysql')->table($db['fsctaccount'].'.'.'ppr_detail')->where('ppr_headid',$arrvalue[0]['ppr_headid'])->update(['status'=>99]);

            $model2 = DB::connection('mysql')->table($db['fsctaccount'].'.'.'ppr_detail')->insert($arrvalue);


        }

        return 1;


    }



    public function getdatavendordetail(){
        $data= Input::all();
        $db = Connectdb::Databaseall();
        $idhead = $data['id'];
        $sql ='SELECT * FROM '.$db['fsctaccount'].'.ppr_head WHERE id = "'.$idhead.'" ';
        $model = DB::connection('mysql')->select($sql);


        return $model;
    }

    public function getdatavendorpprdetail(){
        $data= Input::all();
        $db = Connectdb::Databaseall();
        $idhead = $data['id'];
        $sql ='SELECT * FROM '.$db['fsctaccount'].'.ppr_detail WHERE ppr_headid = "'.$idhead.'"   AND status = "1" ';
        $model = DB::connection('mysql')->select($sql);

        return $model;
    }


}