/**
 * Created by pacharapol on 1/14/2018 AD.
 */
var index = 1;
var amount = 0;
var price = 0;
var vat = 0;
$(document).ready(function() {
    $('#example').DataTable();
    $('.select2').select2();

    $('#addrow').on("click", function () {
       // console.log(index);
       CloneTableRow();
        //bindDataWithStep();
    });

    $(".datepicker").datepicker({
        format: 'dd-mm-yyyy',
    });
});

function getdatesubmit() {

    var valid = $('#configFormvendors').validator('validate').has('.has-error').length;
    if (valid == 0) {
        Savehead();
    }
    return false;
}

function Savehead() {

    var dataposthead = { 'id':$('#id').val(),
                        'date':$('#date').val(),
                        'year':$('#year').val(),
                        'year_th':$('#year_th').val(),
                        'bbr_no_branch':$('#bbr_no_branch').val(),
                        'branch_id':$('#branch_id').val(),
                        'number_ppr':$('#number_ppr').val(),
                        'supplier_id':$('#supplier_id').val(),
                        'type_pay':$('#type_pay').val(),
                        'type_buy':$('#type_buy').val(),
                        'in_house':$('input[name^="in_house"]:checked').val(),
                        'in_budget':$('input[name^="in_budget"]:checked').val(),
                        'urgent_status':$('input[name^="urgent_statu"]:checked').val(),
                        'code_emp':$('#code_emp').val(),
                        'code_sup':$('#code_sup').val(),
                        'status':$('#status').val(),
    };

    var idlastinsert ;

   // console.log(dataposthead);
    $.post('insertheadppr', {data: dataposthead}, function (res) {
          console.log(res);
        idlastinsert = res;
        Savedetail(idlastinsert);
    });


    //Savedetail()

}
function Savedetail(idlastinsert) {
    var arrconfig_group_supp_id = [];
    var arrlist = [];
    var arramount = [];
    var arrtype_amount = [];
    var arrprice = [];
    var arrvat = [];
    var arrtotal = [];
    var arrdaterecentpurchases = [];
    var arravg = [];
    var arrnote = [];


    $('select[name^="config_group_supp_id[]"]').each(function() {
        arrconfig_group_supp_id.push($(this).val());
    });
    $('input[name^="list[]"]').each(function() {
        arrlist.push($(this).val());
    });
    $('input[name^="amount[]"]').each(function() {
        arramount.push($(this).val());
    });
    $('input[name^="type_amount[]"]').each(function() {
        arrtype_amount.push($(this).val());
    });
    $('input[name^="price[]"]').each(function() {
        arrprice.push($(this).val());
    });
    $('select[name^="vat[]"]').each(function() {
        arrvat.push($(this).val());
    });
    $('input[name^="total[]"]').each(function() {
        arrtotal.push($(this).val());
    });
    $('input[name^="daterecentpurchases[]"]').each(function() {
        arrdaterecentpurchases.push($(this).val());
    });
    $('input[name^="avg[]"]').each(function() {
        arravg.push($(this).val());
    });
    $('input[name^="note[]"]').each(function() {
        arrnote.push($(this).val());
    });


    var datapostdetail = { 'config_group_supp_id':arrconfig_group_supp_id,
                     'arrlist':arrlist,
                     'arramount':arramount,
                     'arrtype_amount':arrtype_amount,
                     'arrprice':arrprice,
                     'arrvat':arrvat,
                     'arrtotal':arrtotal,
                     'arrdaterecentpurchases':arrdaterecentpurchases,
                     'arravg':arravg,
                     'arrnote':arrnote,
                     'idhead':idlastinsert,
                };

        console.log(datapostdetail);
    $.post('insertdetailppr', {data: datapostdetail}, function (res) {
         //console.log(res);
        if (res == 1) {
            bootbox.alert({
                title: "แจ้งเตือน",
                message: "บันทึกข้อมูลเรียบร้อยแล้ว!!",
                callback: function () {
                    // $('#ConfirmDialog').modal('hide');
                    $('#myModal').modal('hide');
                    location.reload();

                }
            });
        }

    });



}
//
function insertnew() {

    while (index >= 1) {
        if(index == 1){
            $('#tbindex'+index).remove();
            // index++;
        }else{
            $('#tbindex'+index).remove();

        }
        index--;
    }
    index = 1 ;

    $('#id').val('');
    $('#dateshow').val($('#dateshowset').val());
    $('#date').val($('#dateset').val());
    $('#year').val($('#yearset').val());
    $('#year_th').val($('#year_thset').val());
    $('#bbr_no_branch').val($('#bbr_no_branchset').val());
    $('#number_ppr').val($('#number_pprset').val());


    $('#supplier_id').val('').trigger('change');
    $('#type_pay').val('');
    $('#type_buy').val('');
    $("#in_house1").prop("checked", true);
    $("#in_budget1").prop("checked", true);
    $("#urgent_status2").prop("checked", true);

    $('#config_group_supp_id0').val('');
    $('#list0').val('');
    $('#amount0').val('');
    $('#type_amount0').val('');
    $('#price0').val('');
    $('#vat0').val(0);
    $('#total0').val('');
    $('#daterecentpurchases0').val('');
    $('#avg0').val('');
    $('#note0').val('');

    $(".datepicker").datepicker({
        format: 'dd-mm-yyyy',
    });


}



function CloneTableRow() {

   // console.log(index);

    var html = '';
    html += '<tr id="tbindex'+index+'">';
    html += '<td><select name="config_group_supp_id[]" id="config_group_supp_id'+index+'" class="form-control" required></select></td>';
    html += '<td><input type="text" name="list[]" id="list'+index+'"  class="form-control"></td>';
    html += '<td><input type="text" name="amount[]" id="amount'+index+'" onblur="getamount(this,'+index+')"   class="form-control" style="width: 90px;" placeholder="จำนวน"></td>';
    html += '<td><input type="text" name="type_amount[]" id="type_amount'+index+'"  class="form-control" style="width: 80px;" placeholder="หน่วยนับ"></td>';
    html += '<td><input type="text" name="price[]" id="price'+index+'" disabled onblur="getprice(this,'+index+')"   class="form-control" ></td>';
    html += '<td><select name="vat[]" id="vat'+index+'" onchange="calculatevat(this,'+index+')" class="form-control" style="width: 90px;" ></select></td>';
    html += '<td><input type="text" name="total[]" id="total'+index+'"   class="form-control"  ></td>';
    html += '<td><input type="text" name="daterecentpurchases[]" readonly id="daterecentpurchases'+index+'"   class="form-control datepicker"  ></td>';
    html += '<td><input type="text" name="avg[]" id="avg'+index+'"   class="form-control"  ></td>';
    html += '<td><input type="text" name="note[]" id="note'+index+'"   class="form-control"  ></td>';
    html += '<td><button type="button" id="del" onclick="deleteMe(this);" class="btn btn-danger"><i class="fa fa-trash"></i> </button></td>';
    html += '</tr>';


    $('#tdbody').append(html);
    setoption(index);
    index++;
    $(".datepicker").datepicker({
        format: 'dd-mm-yyyy',
    });

}


function deleteMe(me) {
    var tr = $(me).closest("tr");
    tr.remove();
    // console.log(me);
}

function setoption(index) {

    $('#config_group_supp_id0').find('option').clone().appendTo('#config_group_supp_id'+index+'');
    $('#vat0').find('option').clone().appendTo('#vat'+index+'');

}


function getamount(val,index) {
    $('#total'+index).val(0);

     amount = val.value;
    resulttotalindex(index);
    $('#total'+index).val(0);
    $('#price'+index).prop("disabled", false);

}

function getprice(val,index) {
    $('#total'+index).val(0);
    price = val.value;
    resulttotalindex(index);
}

function calculatevat(val,index) {
    var tex = val.value;
    var setamount = $('#amount'+index).val();
    var setprice = $('#price'+index).val();

    if(tex==0){
        var total = (setamount*setprice)+0;
        total = total.toFixed(2);
        $('#total'+index).val(total);
    }else{
        var total = (setamount*setprice)+((setamount*setprice)/tex);
        total = total.toFixed(2);
        $('#total'+index).val(total);
    }



}


function resulttotalindex(index) {
    $('#total'+index).val(0);
    var total = (amount*price);
    $('#total'+index).val(total);



}





function getdata(id) {
    //console.log(index);

    while (index >= 1) {
        if(index == 1){
            $('#tbindex'+index).remove();
            // index++;
        }else{
            $('#tbindex'+index).remove();

        }
        index--;
    }



    $('#id').val(id);
    $.get("getdatavendordetail?id="+id, function( data ) {

       // console.log(data);
            $('#statusprocess').val(1);


            $('#id').val(data[0].id);
            $('#date').val(data[0].date);
            $('#year').val(data[0].year);
            $('#year_th').val(data[0].year_th);
            $('#bbr_no_branch').val(data[0].bbr_no_branch);
            $('#branch_id').val(data[0].branch_id);
            $('#number_ppr').val(data[0].number_ppr);
            $('#supplier_id').val(data[0].supplier_id).trigger('change');
            $('#type_pay').val(data[0].type_pay);
            $('#type_buy').val(data[0].type_buy);
            // $('input[name^="in_house"]:checked').val();
            // $('input[name^="in_budget"]:checked').val();
            // $('input[name^="urgent_statu"]:checked').val();
            if(data[0].in_house==1){
                $("#in_house1").prop("checked", true);
            }else{
                $("#in_house2").prop("checked", true);

            }

            if(data[0].in_budget==1){
                $("#in_budget1").prop("checked", true);
            }else{
                $("#in_budget2").prop("checked", true);

            }

            if(data[0].urgent_status==1){
                $("#urgent_status1").prop("checked", true);
            }else{
                $("#urgent_status2").prop("checked", true);

            }

            $('#code_emp').val(data[0].code_emp);
            $('#code_sup').val(data[0].code_sup);
            $('#status').val(data[0].status);


            showdetailppr(id);

            //console.log(index);


    });
}


function showdetailppr(idhead) {
    $.get("getdatavendorpprdetail?id="+idhead, function(res) {
        showtable(res);
    });
}

function showtable(data) {

    var count = (Object.keys(data).length);
    console.log(count);
    var html = '';
    // index = 1;
    //
    var set = '';

        for (set = 1; set <= count-1; set++) {
             index++;
            html += '<tr id="tbindex'+index+'">';
            html += '<td><select name="config_group_supp_id[]" id="config_group_supp_id' + index + '" class="form-control"  required></select></td>';
            html += '<td><input type="text" name="list[]" id="list' + index + '"  class="form-control"></td>';
            html += '<td><input type="text" name="amount[]" id="amount' + index + '" onblur="getamount(this,' + index + ')"   class="form-control" style="width: 90px;" placeholder="จำนวน"></td>';
            html += '<td><input type="text" name="type_amount[]" id="type_amount' + index + '"  class="form-control" style="width: 80px;" placeholder="หน่วยนับ"></td>';
            html += '<td><input type="text" name="price[]" id="price' + index + '" disabled onblur="getprice(this,' + index + ')"   class="form-control" ></td>';
            html += '<td><select name="vat[]" id="vat' + index + '" onchange="calculatevat(this,' + index + ')" class="form-control" style="width: 90px;" ></select></td>';
            html += '<td><input type="text" name="total[]" id="total' + index + '"   class="form-control"  ></td>';
            html += '<td><input type="text" name="daterecentpurchases[]" readonly id="daterecentpurchases' + index + '"   class="form-control datepicker"  ></td>';
            html += '<td><input type="text" name="avg[]" id="avg' + index + '"   class="form-control"  ></td>';
            html += '<td><input type="text" name="note[]" id="note' + index + '"   class="form-control"  ></td>';
            html += '<td><button type="button" id="del" onclick="deleteMe(this);" class="btn btn-danger"><i class="fa fa-trash"></i> </button></td>';
            html += '</tr>';
            $('#tdbody').append(html);
            setoption(index);
            $(".datepicker").datepicker({
                format: 'dd-mm-yyyy',
            });


        }


   // console.log(data);

    $.each(data, function(key, value) {
        $('#config_group_supp_id'+key).val(data[key].config_group_supp_id);
        $('#list'+key).val(data[key].list);
        $('#amount'+key).val(data[key].amount);
        $('#type_amount'+key).val(data[key].type_amount);
        $('#price'+key).val(data[key].price);
        $('#vat'+key).val(data[key].vat);
        $('#total'+key).val(data[key].total);
        $('#daterecentpurchases'+key).val(data[key].daterecentpurchases);
        $('#avg'+key).val(data[key].avg);
        $('#note'+key).val(data[key].note);
        index++;

    });





}
