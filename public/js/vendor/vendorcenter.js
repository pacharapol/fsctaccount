/**
 * Created by pacharapol on 1/14/2018 AD.
 */
$(document).ready(function() {
    $('#example').DataTable();

});

function getdatesubmit() {

    var valid = $('#configFormvendors').validator('validate').has('.has-error').length;
    if (valid == 0) {
        Save();
    }
    return false;
}

function Save() {
    var dataInpus = null;

    dataInpus = $('#configFormvendors').serializeArray();

    var data = JSON.stringify(dataInpus);

    // console.log(data);

    $.post('vendorcenterinsertandupdate', {data: data}, function (res) {
        console.log(res);
        if (res == 1) {
            bootbox.alert({
                title: "แจ้งเตือน",
                message: "บันทึกข้อมูลเรียบร้อยแล้ว!!",
                callback: function () {
                    // $('#ConfirmDialog').modal('hide');
                    $('#myModal').modal('hide');
                    location.reload();

                }
            });
        }
    });

}
//
function insertnew() {
    $('#id').val('');
}

function getdata(id) {
    $('#id').val(id);
    $.get("getdatavendorcenter?id="+id, function( data ) {
        // console.log(data)
        $('#id').val(data[0].id);
        $('#pre').val(data[0].pre);
        $('#name_supplier').val(data[0].name_supplier);
        $('#type_branch').val(data[0].type_branch);
        $('#address').val(data[0].address);
        $('#district').val(data[0].district);
        $('#amphur').val(data[0].amphur);
        $('#province').val(data[0].province);
        $('#zipcode').val(data[0].zipcode);
        $('#phone').val(data[0].phone);
        $('#mobile').val(data[0].mobile);
        $('#email').val(data[0].email);
        $('#tax_id').val(data[0].tax_id);
        $('#terms_id').val(data[0].terms_id);
        $('#Note').val(data[0].Note);

    });
}