$(document).ready(function() {
    $('#example').DataTable();
    $('.select2').select2();

});

//
function getdatesubmit() {

    var valid = $('#configFormterms').validator('validate').has('.has-error').length;
    if (valid == 0) {
        Save();
    }
    return false;
}

function Save() {
    var dataInpus = null;

    dataInpus = $('#configFormterms').serializeArray();

    var data = JSON.stringify(dataInpus);

    // console.log(data);

    $.post('configconfiggroupsuppinsertandupdate', {data: data}, function (res) {
        console.log(res);
        if (res == 1) {
            bootbox.alert({
                title: "แจ้งเตือน",
                message: "บันทึกข้อมูลเรียบร้อยแล้ว!!",
                callback: function () {
                    // $('#ConfirmDialog').modal('hide');
                    $('#myModal').modal('hide');
                    location.reload();

                }
            });
        }
    });

}
//
function insertnew() {
    $('#id').val('');
}

function getdata(id) {
    $('#id').val(id);
    $.get("getdataconfigconfiggroupsupp?id="+id, function( data ) {
         console.log(data);
        $('#id').val(data[0].id);
        $('#name').val(data[0].name);
        $('#status').val(data[0].status);
        $('#tax_config_id').val(data[0].tax_config_id);

    });
}
