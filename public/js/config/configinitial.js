$(document).ready(function() {
    $('#example').DataTable();

});

//
function getdatesubmit() {

    var valid = $('#configFromInitial').validator('validate').has('.has-error').length;
    if (valid == 0) {
        Save();
    }
    return false;
}

function Save() {
    var dataInpus = null;

    dataInpus = $('#configFromInitial').serializeArray();

    var data = JSON.stringify(dataInpus);

    // console.log(data);

    $.post('configinitialinsertandupdate', {data: data}, function (res) {
        console.log(res);
        if (res == 1) {
            bootbox.alert({
                title: "แจ้งเตือน",
                message: "บันทึกข้อมูลเรียบร้อยแล้ว!!",
                callback: function () {
                    // $('#ConfirmDialog').modal('hide');
                    $('#myModal').modal('hide');
                    location.reload();

                }
            });
        }
    });

}
//
function insertnew() {
    $('#id').val('');
}

function getdata(id) {
    $('#id').val(id);
    $.get("getdataconfiginitial?id="+id, function( data ) {
        // console.log(data)
        $('#id').val(data[0].id);
        $('#per').val(data[0].per);
        $('#status').val(data[0].status);
    });
}