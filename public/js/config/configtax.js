/**
 * Created by pacharapol on 1/14/2018 AD.
 */
$(document).ready(function() {


});


function getdatesubmit() {

    var valid = $('#configFormtax').validator('validate').has('.has-error').length;
    if (valid == 0) {
        Saveconfigtax();
    }
    return false;
}

function Saveconfigtax() {
    var configtaxInpus = null;

    configtaxInpus = $('#configFormtax').serializeArray();

    var data = JSON.stringify(configtaxInpus);

   // console.log(data);

    $.post('configtaxinsertandupdate', {data: data}, function (res) {
       // console.log(res);
        if (res == 1) {
            bootbox.alert({
                title: "แจ้งเตือน",
                message: "บันทึกข้อมูลเรียบร้อยแล้ว!!",
                callback: function () {
                    // $('#ConfirmDialog').modal('hide');
                    $('#myModal').modal('hide');
                    location.reload();

                }
            });
        }
    });

}
//
function insertnew() {
    $('#id').val('');
}

function getdata(id) {
    $('#id').val(id);
    $.get("getdataconfigtax?id="+id, function( data ) {
        $('#id').val(data[0].id);
        $('#tax').val(data[0].tax);
        $('#tax_detail').val(data[0].tax_detail);
        $('#status').val(data[0].status);
    });
}